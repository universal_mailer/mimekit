//
//  AppDelegate.h
//  MIMEKit OS X Test Application
//
//  Created by luca on 27/10/13.
//  Copyright (c) 2013 Portable Knowledge, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;

@end
