//
//  AppDelegate.m
//  MIMEKit OS X Test Application
//
//  Created by luca on 27/10/13.
//  Copyright (c) 2013 Portable Knowledge, LLC. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // Insert code here to initialize your application
}

@end
