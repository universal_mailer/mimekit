//
//  AppDelegate.h
//  MIMEKit iOS Test Application
//
//  Created by luca on 27/10/13.
//  Copyright (c) 2013 Portable Knowledge, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
